

function problem1(inventory){
    if(!(Array.isArray(inventory))){
        return console.log("Array not found");
    }
    

    if(inventory.length == 0){
        return console.log("Data Not Present");
    }
    
    const year_inventory=[];

    inventory.map(function(car){
        year_inventory.push(car.car_year);
    })

    return year_inventory;
}

export default problem1;