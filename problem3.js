

function comp(a,b){

    return (a.car_model.localeCompare(b.car_model))

}
function problem3(inventory){

    if(!(Array.isArray(inventory))){
        return console.log("Array not found");
    }
    
    
    if(inventory.length == 0){
        return console.log("Data Not Present");
    }

    const sortedInventory = inventory.sort(comp);

    console.log(sortedInventory);
}

export default problem3;