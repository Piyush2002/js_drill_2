

function problem1(inventory){
    if(!(Array.isArray(inventory))){
        return console.log("Array not found");
    }
    

    if(inventory.length == 0){
        return console.log("Data Not Present");
    }

    // using reduce 
    // inventory.reduce(function(accumulator,current){

    //     if(current.id===33){
    //         console.log(`Car 33 is a ${current.car_year} ${current.car_make} ${current.car_model}`)
    //     }

    // },{})

    // using filter
    inventory.filter(function(car){

        if(car.id === 33 ){
            console.log(`Car 33 is a ${car.car_year} ${car.car_make} ${car.car_model}`);
        }

    })
}

export default problem1;