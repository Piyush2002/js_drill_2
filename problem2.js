

function problem1(inventory){
    if(!(Array.isArray(inventory))){
        return console.log("Array not found");
    }
    

    if(inventory.length == 0){
        return console.log("Data Not Present");
    }

    inventory.filter(function(car){

        if(car.id === inventory.length ){
            console.log(`Last car is a ${car.car_make} ${car.car_model}`);
        }

    })
}

export default problem1;